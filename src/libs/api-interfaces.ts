export interface User {
  id: string;
  name: string;
  group: string;
}
export interface Group {
  id: string;
  name: string;
  department: string;
}
