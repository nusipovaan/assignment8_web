import { AdminService } from './../admin.service';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/libs/api-interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {


  users$: Observable<User[]>;
  constructor(
    private adminService: AdminService
  ) {}

  ngOnInit(): void {

    this.getData();

  }
  getData(){
    this.users$ = this.adminService.getUsers();
  }

}
