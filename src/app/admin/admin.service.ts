import { User, Group } from './../../libs/api-interfaces';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AdminService {
  constructor(
    private http: HttpClient,
  ){}

  getUsers(): Observable<User[]> {
    return this.http
      .get<User[]>(`http://localhost:3000/users`)
  }

  getGroups(): Observable<Group[]> {
    return this.http
      .get<Group[]>(`http://localhost:3000/groups`)
  }
}
