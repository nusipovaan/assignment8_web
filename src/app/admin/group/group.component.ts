import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Group } from 'src/libs/api-interfaces';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  groups$: Observable<Group[]>;
  constructor(
    private adminService: AdminService) { }

  ngOnInit(): void {
    this.getData();
  }
  getData(){
    this.groups$ = this.adminService.getGroups();
  }

}
