import { GroupComponent } from './group/group.component';
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Route } from '@angular/router';
import { NgModule } from "@angular/core";


const routes: Route[] = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'users',
    component: UserComponent,
  },
  {
    path: 'groups',
    component: GroupComponent,
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule{

}
